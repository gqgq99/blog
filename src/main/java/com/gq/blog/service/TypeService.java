package com.gq.blog.service;

import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.TypeBlogDTO;
import com.gq.blog.pojo.Type;

import java.util.List;

public interface TypeService {
    PageInfo getTypePage(int pageNum,int pageSize);
    Type getTypeById(Long id);
    int updateById(Type type);
    int deleteById(Long id);
    int addType(Type type);
    Type getTypeByName(String name);

    List<Type> getAllType();

    List<TypeBlogDTO> getTypeBlogs();
}

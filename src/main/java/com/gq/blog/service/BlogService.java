package com.gq.blog.service;

import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.BlogDetailDTO;
import com.gq.blog.dto.BlogSearchDTO;
import com.gq.blog.pojo.Blog;

import java.util.List;
import java.util.Map;

public interface BlogService {
    PageInfo<BlogDetailDTO> getBlogPage(BlogSearchDTO blogSearchDTOm, int pageSize);

    int  saveBlog(BlogDetailDTO blogDetailDTO);

    BlogDetailDTO getBlogById(Long id);

    int deleteBlog(Long id);

    PageInfo<BlogDetailDTO> getBlogList(int pageNum, int pageSize);

    List<Blog> getRecommendBlogs(int pageNum, int pageSize);

    PageInfo<BlogDetailDTO> getBlogByQueryKey(int pageNum, int pageSize,String queryKey);

    BlogDetailDTO getBlogByIdAndConvert(Long id);

    PageInfo<BlogDetailDTO> getBlogPageByTageId(Long id, int pageNum, int pageSize);

    Map<String, List<Blog>> archiveBlog();

    int  getBlogCount();
}

package com.gq.blog.service;

import com.gq.blog.pojo.User;

public interface UserService {
    User getUser(String username,String password);
}

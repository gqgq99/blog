package com.gq.blog.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.TypeBlogDTO;
import com.gq.blog.mapper.TypeMapper;
import com.gq.blog.pojo.Type;
import com.gq.blog.pojo.TypeExample;
import com.gq.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    TypeMapper typeMapper;

    @Override
    public PageInfo getTypePage(int pageNum, int pageSize) {
        List<Type> allType = typeMapper.selectAllType();
        PageHelper.startPage(pageNum,pageSize);
        TypeExample typeExample = new TypeExample();
        List<Type> types = typeMapper.selectByExample(typeExample);
        PageInfo<Type> pageInfo = new PageInfo<>(types);
        pageInfo.setPageNum(pageNum);
        pageInfo.setPageSize(pageSize);
        pageInfo.setTotal(allType.size());
        pageInfo.setPages((int)Math.ceil((float)allType.size()/(float)pageSize));
        return pageInfo;
    }

    @Override
    public Type getTypeById(Long id) {
        return typeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateById(Type type) {
        return typeMapper.updateByPrimaryKey(type);
    }

    @Override
    public int deleteById(Long id) {
        return typeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int addType(Type type) {
        return typeMapper.insertSelective(type);
    }

    @Override
    public Type getTypeByName(String name) {
        TypeExample typeExample = new TypeExample();
        typeExample.createCriteria().andNameEqualTo(name);
        List<Type> types = typeMapper.selectByExample(typeExample);
        if (types.size()>0){
            return types.get(0);
        }else {
            return null;
        }
    }

    @Override
    public List<Type> getAllType() {
        TypeExample typeExample = new TypeExample();
        return typeMapper.selectByExample(typeExample);
    }

    @Override
    public List<TypeBlogDTO> getTypeBlogs() {
        return typeMapper.getTypeBlogs();
    }

}

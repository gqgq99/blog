package com.gq.blog.service.impl;

import com.gq.blog.mapper.UserMapper;
import com.gq.blog.pojo.User;
import com.gq.blog.pojo.UserExample;
import com.gq.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public User getUser(String username, String password) {
        UserExample userExample = new UserExample();
        userExample.createCriteria()
                .andUsernameEqualTo(username)
                .andPasswordEqualTo(password);
        List<User> users = userMapper.selectByExample(userExample);
        if(users.size()==1){
            for (User user : users) {
                return user;
            }
        }
            return null;
    }
}

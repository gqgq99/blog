package com.gq.blog.service.impl;

import com.gq.blog.dto.CommentShowDTO;
import com.gq.blog.mapper.CommentMapper;
import com.gq.blog.pojo.Comment;
import com.gq.blog.pojo.CommentExample;
import com.gq.blog.service.CommentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentMapper commentMapper;

    @Override
    public void saveComment(CommentShowDTO commentShowDTO) {
        Long parentCommentId = commentShowDTO.getParentComment().getId();
        if (parentCommentId != -1) {
            commentShowDTO.setParentCommentId(parentCommentId);
            commentShowDTO.setParentComment(findOne(parentCommentId));
        } else {
            commentShowDTO.setParentComment(null);
        }
        commentShowDTO.setCreateTime(new Date());
        Comment comment = new Comment();
        BeanUtils.copyProperties(commentShowDTO, comment);
        commentMapper.insertSelective(comment);
    }

    private Comment findOne(Long parentCommentId) {
        CommentExample commentExample = new CommentExample();
        commentExample.createCriteria().andIdEqualTo(parentCommentId);
        List<Comment> comments = commentMapper.selectByExample(commentExample);
        if (comments.size() > 0) {
            return comments.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<CommentShowDTO> getCommentByBlogId(Long blogId) {
        List<CommentShowDTO> comments = commentMapper.findByBlogIdAndParentCommentId(blogId, (long) -1);
        if (comments.size() > 0) {
            return forComment(comments);
        } else {
            return comments;
        }
    }

    private List<CommentShowDTO> forComment(List<CommentShowDTO> comments) {
        for (CommentShowDTO comment : comments) {
            for (CommentShowDTO replyComment : comment.getReplyComments()) {
                List<CommentShowDTO> byBlogIdAndParentCommentId = commentMapper.findByBlogIdAndParentCommentId(comment.getBlogId(), replyComment.getId());
                if (byBlogIdAndParentCommentId.size() > 0) {
                    for (CommentShowDTO commentShowDTO : byBlogIdAndParentCommentId) {
                        Comment comment1 = new Comment();
                        BeanUtils.copyProperties(replyComment,comment1);
                        commentShowDTO.setParentComment(comment1);
                    }
                    replyComment.setReplyComments(forComment(byBlogIdAndParentCommentId));
                }
            }

        }
        return eachComment(comments);
    }

    /**
     * 循环每个顶级的评论节点
     * @param comments
     * @return
     */
    private List<CommentShowDTO> eachComment(List<CommentShowDTO> comments) {
        List<CommentShowDTO> commentsView = new ArrayList<>();
        for (CommentShowDTO comment : comments) {
            CommentShowDTO c = new CommentShowDTO();
            BeanUtils.copyProperties(comment,c);
            commentsView.add(c);
        }
        //合并评论的各层子代到第一级子代集合中
        combineChildren(commentsView);
        return commentsView;
    }

    /**
     * @param comments root根节点，blog不为空的对象集合
     * @return
     */
    private void combineChildren(List<CommentShowDTO> comments) {

        for (CommentShowDTO comment : comments) {
            List<CommentShowDTO> replys1 = comment.getReplyComments();
            for (CommentShowDTO reply1 : replys1) {
                //循环迭代，找出子代，存放在tempReplys中
                recursively(reply1);
            }
            //修改顶级节点的reply集合为迭代处理后的集合
            comment.setReplyComments(tempReplys);
            //清除临时存放区
            tempReplys = new ArrayList<>();
        }
    }

    //存放迭代找出的所有子代的集合
    private List<CommentShowDTO> tempReplys = new ArrayList<>();

    /**
     * 递归迭代，剥洋葱
     *
     * @param comment 被迭代的对象
     * @return
     */
    private void recursively(CommentShowDTO comment) {
        tempReplys.add(comment);//顶节点添加到临时存放集合
        if (comment.getReplyComments().size() > 0) {
            List<CommentShowDTO> replys = comment.getReplyComments();
            for (CommentShowDTO reply : replys) {
                tempReplys.add(reply);
                if (reply.getReplyComments().size() > 0) {
                    recursively(reply);
                }
            }
        }
    }
}

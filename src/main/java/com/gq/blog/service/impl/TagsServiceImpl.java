package com.gq.blog.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.TagBlogDTO;
import com.gq.blog.mapper.TagMapper;
import com.gq.blog.pojo.Tag;
import com.gq.blog.pojo.TagExample;
import com.gq.blog.pojo.TypeExample;
import com.gq.blog.service.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagsServiceImpl implements TagsService {

    @Autowired
    TagMapper tagMapper;

    @Override
    public PageInfo<Tag> getTagsPage(int pageNum, int pageSize) {
        TagExample tagExample = new TagExample();
        List<Tag> allTag = tagMapper.selectByExample(tagExample);
        PageHelper.startPage(pageNum,pageSize);
        List<Tag> tagPage = tagMapper.selectByExample(tagExample);
        PageInfo<Tag> pageInfo = new PageInfo<>(tagPage);
        pageInfo.setTotal(allTag.size());
        pageInfo.setPageNum(pageNum);
        pageInfo.setPageSize(pageSize);
        pageInfo.setPages((int)Math.ceil((float)allTag.size()/(float)pageSize));
        return pageInfo;
    }

    @Override
    public Tag getTagById(Long id) {
        return tagMapper.selectByPrimaryKey(id);
    }

    @Override
    public Tag getTagByName(String name) {
        TagExample tagExample = new TagExample();
        tagExample.createCriteria().andNameEqualTo(name);
        List<Tag> tags = tagMapper.selectByExample(tagExample);
        if(tags.size()==1){
            return tags.get(0);
        }else {
            return null;
        }
    }

    @Override
    public int addTag(Tag tag) {
        return tagMapper.insertSelective(tag);
    }

    @Override
    public int updateById(Tag tag) {
        return tagMapper.updateByPrimaryKeySelective(tag);
    }

    @Override
    public int deleteById(Long id) {
        return tagMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<Tag> getAllTag() {
        TagExample tagExample = new TagExample();
        return tagMapper.selectByExample(tagExample);
    }

    @Override
    public List<TagBlogDTO> getTagBlogs() {
        return tagMapper.getTagBlogs();
    }
}

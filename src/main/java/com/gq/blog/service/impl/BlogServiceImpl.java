package com.gq.blog.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.BlogDetailDTO;
import com.gq.blog.dto.BlogSearchDTO;
import com.gq.blog.mapper.BlogMapper;
import com.gq.blog.mapper.BlogTagsMapper;
import com.gq.blog.pojo.*;
import com.gq.blog.service.BlogService;
import com.gq.blog.utils.MarkdownUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    BlogMapper blogMapper;
    @Autowired
    BlogTagsMapper blogTagsMapper;
    @Autowired
    HttpSession session;


    @Override
    public PageInfo<BlogDetailDTO> getBlogPage(BlogSearchDTO blogSearchDTO, int pageSize) {
        List<BlogDetailDTO> allBlog = blogMapper.getBlog(blogSearchDTO);
        PageHelper.startPage(Integer.parseInt(blogSearchDTO.getPageNum()), pageSize);
        List<BlogDetailDTO> blogPage = blogMapper.getBlog(blogSearchDTO);
        PageInfo<BlogDetailDTO> pageInfo = new PageInfo<>(blogPage);
        pageInfo.setTotal(allBlog.size());
        pageInfo.setPageNum(Integer.parseInt(blogSearchDTO.getPageNum().toString()));
        pageInfo.setPageSize(pageSize);
        pageInfo.setPages((int) Math.ceil((float) allBlog.size() / (float) pageSize));
        return pageInfo;
    }

    @Override
    public int saveBlog(BlogDetailDTO blogDetailDTO) {
        Blog blog = new Blog();
        if(blogDetailDTO.getId()!=null){
            blogDetailDTO.setUpdateTime(new Date());
            BeanUtils.copyProperties(blogDetailDTO, blog);
            int result = blogMapper.updateByPrimaryKeySelective(blog);
            BlogTagsExample blogTagsExample = new BlogTagsExample();
            blogTagsExample.createCriteria().andBlogsIdEqualTo(blogDetailDTO.getId());
            blogTagsMapper.deleteByExample(blogTagsExample);
            if(result>0){
                if (!blogDetailDTO.getTagIds().isEmpty()) {
                    String[] tageIds = blogDetailDTO.getTagIds().split(",");
                    BlogTags blogTags = new BlogTags();
                    for (String tageId : tageIds) {
                        blogTags.setBlogsId(blog.getId());
                        blogTags.setTagsId(Long.valueOf(tageId));
                        blogTagsMapper.insert(blogTags);
                    }
                }
            }
            return result;
        }
        User user = (User) session.getAttribute("user");
        blogDetailDTO.setCreateTime(new Date());
        blogDetailDTO.setUpdateTime(new Date());
        blogDetailDTO.setViews(0);
        blogDetailDTO.setUserId(user.getId());
        BeanUtils.copyProperties(blogDetailDTO, blog);
        int count = blogMapper.insertSelective(blog);
        if (count > 0) {
            if (!blogDetailDTO.getTagIds().isEmpty()) {
                String[] tageIds = blogDetailDTO.getTagIds().split(",");
                BlogTags blogTags = new BlogTags();
                for (String tageId : tageIds) {
                    blogTags.setBlogsId(blog.getId());
                    blogTags.setTagsId(Long.valueOf(tageId));
                    blogTagsMapper.insert(blogTags);
                }
            }
        }
        return count;
    }

    @Override
    public BlogDetailDTO getBlogById(Long id) {
        BlogDetailDTO blogDetailDTO = blogMapper.getBlogById(id);
        if (blogDetailDTO.getTags().size() > 0) {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < blogDetailDTO.getTags().size(); i++) {
                if (i < blogDetailDTO.getTags().size() - 1) {
                    stringBuffer.append(blogDetailDTO.getTags().get(i).getId())
                            .append(",");
                }
                if (i >= blogDetailDTO.getTags().size() - 1) {
                    stringBuffer.append(blogDetailDTO.getTags().get(i).getId());
                }
            }
            blogDetailDTO.setTagIds(stringBuffer.toString());
        }
        return blogDetailDTO;
    }

    @Override
    public int deleteBlog(Long id) {
        int count = blogMapper.deleteByPrimaryKey(id);
        if(count>0){
            BlogTagsExample blogTagsExample = new BlogTagsExample();
            blogTagsExample.createCriteria().andBlogsIdEqualTo(id);
            blogTagsMapper.deleteByExample(blogTagsExample);
        }
        return count;
    }

    @Override
    public PageInfo<BlogDetailDTO> getBlogList(int pageNum, int pageSize) {
        BlogExample blogExample = new BlogExample();
        List<Blog> allBlog = blogMapper.selectByExample(blogExample);
        PageHelper.startPage(pageNum,pageSize);
        List<BlogDetailDTO> blogDetailDTOS = blogMapper.getBlogList();
        PageInfo<BlogDetailDTO> pageInfo = new PageInfo<>(blogDetailDTOS);
        pageInfo.setTotal(allBlog.size());
        pageInfo.setPageNum(pageNum);
        pageInfo.setPageSize(pageSize);
        pageInfo.setPages((int) Math.ceil((float) allBlog.size() / (float) pageSize));
        System.out.println(pageInfo);
        return pageInfo;
    }

    @Override
    public List<Blog> getRecommendBlogs(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Blog> blogs = blogMapper.getRecommendBlogs();
        return blogs;
    }

    @Override
    public PageInfo<BlogDetailDTO> getBlogByQueryKey(int pageNum, int pageSize,String queryKey) {
        List<BlogDetailDTO> allBlog = blogMapper.getBlogByQueryKey(queryKey);
        PageHelper.startPage(pageNum,pageSize);
        List<BlogDetailDTO> blogByQueryKey = blogMapper.getBlogByQueryKey(queryKey);
        PageInfo<BlogDetailDTO> pageInfo = new PageInfo<>(blogByQueryKey);
        pageInfo.setTotal(allBlog.size());
        pageInfo.setPageNum(pageNum);
        pageInfo.setPageSize(pageSize);
        pageInfo.setPages((int) Math.ceil((float) allBlog.size() / (float) pageSize));
        System.out.println(pageInfo);
        return pageInfo;
    }

    @Override
    public BlogDetailDTO getBlogByIdAndConvert(Long id) {
        BlogDetailDTO blogById = blogMapper.getBlogById(id);
        String content = blogById.getContent();
        blogById.setViews(blogById.getViews()+1);

        blogMapper.updateByPrimaryKeySelective(blogById);
        blogById.setContent(MarkdownUtils.markdownToHtmlExtensions(content));
        return blogById;
    }

    @Override
    public PageInfo<BlogDetailDTO> getBlogPageByTageId(Long id, int pageNum, int pageSize) {
        List<BlogDetailDTO> blogDetailDTOS = blogMapper.getBlogPageByTageId(id);
        PageHelper.startPage(pageNum,pageSize);
        List<BlogDetailDTO> pageByTageId = blogMapper.getBlogPageByTageId(id);
        PageInfo<BlogDetailDTO> pageInfo = new PageInfo<>(pageByTageId);
        pageInfo.setTotal(blogDetailDTOS.size());
        pageInfo.setPageNum(pageNum);
        pageInfo.setPageSize(pageSize);
        pageInfo.setPages((int) Math.ceil((float) blogDetailDTOS.size() / (float) pageSize));
        return pageInfo;
    }

    @Override
    public Map<String, List<Blog>> archiveBlog() {
        List<String> years = blogMapper.getBlogYear();
        Map<String,List<Blog>> map = new HashMap<>();
        for (String year : years) {
            List<Blog>blogs = blogMapper.getBlogByTime(year);
            map.put(year,blogs);
        }
        return map;
    }

    @Override
    public int getBlogCount() {

       return blogMapper.getBlogCount();
    }
}

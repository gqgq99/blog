package com.gq.blog.service;

import com.gq.blog.dto.CommentShowDTO;

import java.util.List;

public interface CommentService {
    void saveComment(CommentShowDTO commentShowDTO);

    List<CommentShowDTO> getCommentByBlogId(Long blogId);
}

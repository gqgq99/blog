package com.gq.blog.service;

import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.TagBlogDTO;
import com.gq.blog.pojo.Tag;

import java.util.List;

public interface TagsService {
    PageInfo<Tag> getTagsPage(int pageNum,int pageSize);

    Tag getTagById(Long id);

    Tag getTagByName(String name);

    int addTag(Tag tag);

    int updateById(Tag tag);

    int deleteById(Long id);

    List<Tag> getAllTag();

    List<TagBlogDTO> getTagBlogs();
}

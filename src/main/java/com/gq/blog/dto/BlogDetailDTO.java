package com.gq.blog.dto;

import com.gq.blog.pojo.Blog;
import com.gq.blog.pojo.Tag;

import java.util.List;

public class BlogDetailDTO extends Blog {
    private String typeName;
    private String tagIds;
    private List<Tag> tags;
    private String avatar;
    private String username;
    private String nickname;

    public BlogDetailDTO() {
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTagIds() {
        return tagIds;
    }

    public void setTagIds(String tagIds) {
        this.tagIds = tagIds;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "BlogDetailDTO{" +
                "typeName='" + typeName + '\'' +
                ", tagIds='" + tagIds + '\'' +
                ", tags=" + tags +
                ", avatar='" + avatar + '\'' +
                ", username='" + username + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}

package com.gq.blog.dto;

import com.gq.blog.pojo.Blog;
import com.gq.blog.pojo.Type;

import java.util.List;

public class TypeBlogDTO extends Type {
   private List<Blog> blogs;

    public TypeBlogDTO() {
    }

    public TypeBlogDTO(List<Blog> blogs) {
        this.blogs = blogs;
    }

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }
}

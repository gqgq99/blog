package com.gq.blog.dto;

import com.gq.blog.pojo.Blog;
import com.gq.blog.pojo.Comment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommentShowDTO {
    private Long id;
    private String avatar;
    private String content;
    private Date createTime;
    private String email;
    private String nickname;
    private Comment parentComment;
    private List<CommentShowDTO> replyComments = new ArrayList<>();
    private boolean adminComment;
    private Blog blog;
    private Long blogId;
    private Long parentCommentId;

    public CommentShowDTO() {
    }

    public CommentShowDTO(Long id, String avatar, String content, Date createTime, String email, String nickname, Comment parentComment, List<CommentShowDTO> replyComments, boolean adminComment, Blog blog, Long blogId, Long parentCommentId) {
        this.id = id;
        this.avatar = avatar;
        this.content = content;
        this.createTime = createTime;
        this.email = email;
        this.nickname = nickname;
        this.parentComment = parentComment;
        this.replyComments = replyComments;
        this.adminComment = adminComment;
        this.blog = blog;
        this.blogId = blogId;
        this.parentCommentId = parentCommentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Comment getParentComment() {
        return parentComment;
    }

    public void setParentComment(Comment parentComment) {
        this.parentComment = parentComment;
    }

    public List<CommentShowDTO> getReplyComments() {
        return replyComments;
    }

    public void setReplyComments(List<CommentShowDTO> replyComments) {
        this.replyComments = replyComments;
    }

    public boolean isAdminComment() {
        return adminComment;
    }

    public void setAdminComment(boolean adminComment) {
        this.adminComment = adminComment;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public Long getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(Long parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    @Override
    public String toString() {
        return "CommentShowDTO{" +
                "id=" + id +
                ", avatar='" + avatar + '\'' +
                ", content='" + content + '\'' +
                ", createTime=" + createTime +
                ", email='" + email + '\'' +
                ", nickname='" + nickname + '\'' +
                ", parentComment=" + parentComment +
                ", replyComments=" + replyComments +
                ", adminComment=" + adminComment +
                ", blog=" + blog +
                ", blogId=" + blogId +
                ", parentCommentId=" + parentCommentId +
                '}';
    }
}

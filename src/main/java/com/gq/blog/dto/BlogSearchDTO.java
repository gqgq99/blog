package com.gq.blog.dto;

public class BlogSearchDTO {
    private Boolean recommend=true;
    private String title;
    private Long typeId;
    private String pageNum;
    private int pageSize;

    public BlogSearchDTO() {
    }

    public Boolean getRecommend() {
        return recommend;
    }

    public void setRecommend(Boolean recommend) {
        this.recommend = recommend;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public BlogSearchDTO(Boolean recommend, String title, Long typeId, String pageNum, int pageSize) {
        this.recommend = recommend;
        this.title = title;
        this.typeId = typeId;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "BlogSearchDTO{" +
                "recommend=" + recommend +
                ", title='" + title + '\'' +
                ", typeId=" + typeId +
                ", pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                '}';
    }
}

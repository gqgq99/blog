package com.gq.blog.dto;

import com.gq.blog.pojo.Blog;
import com.gq.blog.pojo.Tag;

import java.util.List;

public class TagBlogDTO extends Tag {
    private List<Blog> blogs;

    public TagBlogDTO() {
    }

    public TagBlogDTO(List<Blog> blogs) {
        this.blogs = blogs;
    }

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }
}

package com.gq.blog.dto;

public class ArchivesYearDTO {
    private String year;

    public ArchivesYearDTO() {
    }

    public ArchivesYearDTO(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}

package com.gq.blog.controller;

import com.github.pagehelper.PageInfo;
import com.gq.blog.pojo.Tag;
import com.gq.blog.service.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/admin/tags")
public class TagsController {

    @Autowired
    TagsService tagsService;

    @RequestMapping(value = "",method = RequestMethod.GET)
    public String getTagsPage(@RequestParam int pageNum, Model model){
        int pageSize = 10;
        PageInfo<Tag> tagsPage = tagsService.getTagsPage(pageNum, pageSize);
        model.addAttribute("page",tagsPage);
        return "admin/tags";
    }

    @RequestMapping(value = "/input",method = RequestMethod.GET)
    public String tagsInput(Model model){
        model.addAttribute("tag",new Tag());
        return "admin/tags-input";
    }

    @RequestMapping(value = "/{id}/input", method = RequestMethod.GET)
    public String getTypeById(@PathVariable Long id, Model model) {
        Tag tagById = tagsService.getTagById(id);
        model.addAttribute("tag", tagById);
        return "admin/tags-input";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addType(@Valid Tag tag, BindingResult result, RedirectAttributes attributes) {
      Tag tagByName= tagsService.getTagByName(tag.getName());
        if (tagByName != null) {
            result.rejectValue("name", "nameError", "此标签已存在");
        }
        if (result.hasErrors()) {
            return "admin/tags-input";
        }
        int count = tagsService.addTag(tag);
        if (count == 1) {
            attributes.addFlashAttribute("message", "新增成功");
        } else {
            attributes.addFlashAttribute("message", "新增失败");
        }
        return "redirect:/admin/tags?pageNum=1";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateType(@Valid Tag tag, BindingResult result, RedirectAttributes attributes) {
        Tag tagByName = tagsService.getTagByName(tag.getName());
        if (tagByName != null) {
            result.rejectValue("name", "nameError", "此标签已存在");
        }
        if (result.hasErrors()) {
            return "admin/tags-input";
        }
        int count = tagsService.updateById(tag);
        if (count == 1) {
            attributes.addFlashAttribute("message", "修改成功");
        } else {
            attributes.addFlashAttribute("message", "修改失败");
        }
        return "redirect:/admin/tags?pageNum=1";
    }

    @RequestMapping(value = "/{id}/delete",method = RequestMethod.GET)
    public String delete(@PathVariable Long id, RedirectAttributes attributes){
        int count = tagsService.deleteById(id);
        if (count == 1) {
            attributes.addFlashAttribute("message", "删除成功");
        } else {
            attributes.addFlashAttribute("message", "删除失败");
        }
        return "redirect:/admin/tags?pageNum=1";
    }
}

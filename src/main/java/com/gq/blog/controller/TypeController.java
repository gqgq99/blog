package com.gq.blog.controller;

import com.github.pagehelper.PageInfo;
import com.gq.blog.pojo.Type;
import com.gq.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/admin/types")
public class TypeController {

    @Autowired
    TypeService typeService;

    @RequestMapping(value = "")
    public String types(@RequestParam int pageNum, Model model) {
        int pageSize = 5;
        PageInfo pageInfo = typeService.getTypePage(pageNum, pageSize);
        model.addAttribute("page", pageInfo);
        System.out.println(pageInfo);
        return "admin/types";
    }

    @RequestMapping(value = "/input", method = RequestMethod.GET)
    public String input(Model model) {
        model.addAttribute("type", new Type());
        return "/admin/types-input";
    }

    @RequestMapping(value = "/{id}/input", method = RequestMethod.GET)
    public String getTypeById(@PathVariable Long id, Model model) {
        Type typeById = typeService.getTypeById(id);
        model.addAttribute("type", typeById);
        return "/admin/types-input";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addType(@Valid Type type, BindingResult result, RedirectAttributes attributes) {
        Type typeByName = typeService.getTypeByName(type.getName());
        if (typeByName != null) {
            result.rejectValue("name", "nameError", "此分类已存在");
        }
        if (result.hasErrors()) {
            return "admin/types-input";
        }
        int count = typeService.addType(type);
        if (count == 1) {
            attributes.addFlashAttribute("message", "新增成功");
        } else {
            attributes.addFlashAttribute("message", "新增失败");
        }
        return "redirect:/admin/types?pageNum=1";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateType(@Valid Type type, BindingResult result, RedirectAttributes attributes) {
        Type typeByName = typeService.getTypeByName(type.getName());
        if (typeByName != null) {
            result.rejectValue("name", "nameError", "此分类已存在");
        }
        if (result.hasErrors()) {
            return "admin/types-input";
        }
        int count = typeService.updateById(type);
        if (count == 1) {
            attributes.addFlashAttribute("message", "修改成功");
        } else {
            attributes.addFlashAttribute("message", "修改失败");
        }
        return "redirect:/admin/types?pageNum=1";
    }

    @RequestMapping(value = "/{id}/delete",method = RequestMethod.GET)
    public String delete(@PathVariable Long id, RedirectAttributes attributes){
        int count = typeService.deleteById(id);
        if (count == 1) {
            attributes.addFlashAttribute("message", "删除成功");
        } else {
            attributes.addFlashAttribute("message", "删除失败");
        }
        return "redirect:/admin/types?pageNum=1";
    }
}

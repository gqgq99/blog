package com.gq.blog.controller;

import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.BlogDetailDTO;
import com.gq.blog.dto.TagBlogDTO;
import com.gq.blog.pojo.Blog;
import com.gq.blog.service.BlogService;
import com.gq.blog.service.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class TagShowController {

    @Autowired
    TagsService tagsService;
    @Autowired
    BlogService blogService;

    @RequestMapping(value = "/tags/{id}/{pageNum}",method = RequestMethod.GET)
    public String tagPage(@PathVariable Long id, @PathVariable int pageNum, Model model){
        int pageSize = 8;
        List<TagBlogDTO> tagBlogs = tagsService.getTagBlogs();
        if(id==-1){
            id=tagBlogs.get(0).getId();
        }
        PageInfo<BlogDetailDTO> pageInfo =blogService.getBlogPageByTageId(id,pageNum,pageSize);
        model.addAttribute("tags",tagBlogs);
        model.addAttribute("page",pageInfo);
        model.addAttribute("activeTagId", id);
        return "/tags";
    }
}

package com.gq.blog.controller;

import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.BlogDetailDTO;
import com.gq.blog.dto.BlogSearchDTO;
import com.gq.blog.pojo.Tag;
import com.gq.blog.pojo.Type;
import com.gq.blog.service.BlogService;
import com.gq.blog.service.TagsService;
import com.gq.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/admin/blogs")
public class BlogController {

    @Autowired
    BlogService blogService;
    @Autowired
    TypeService typeService;
    @Autowired
    TagsService tagsService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String blogs(BlogSearchDTO blogSearchDTO, Model model) {
        int pageSize = 10;
        PageInfo<BlogDetailDTO> pageInfo = blogService.getBlogPage(blogSearchDTO, pageSize);
        model.addAttribute("page", pageInfo);
        List<Type> types = typeService.getAllType();
        model.addAttribute("types",types);
        return "admin/blogs";
    }

    @RequestMapping(value = "/search")
    public String searchBlog(BlogSearchDTO blogSearchDTO, Model model) {
        int pageSize = 10;
        PageInfo<BlogDetailDTO> pageInfo = blogService.getBlogPage(blogSearchDTO, pageSize);
        model.addAttribute("page", pageInfo);
        List<Type> types = typeService.getAllType();
        model.addAttribute("types",types);
        return "admin/blogs :: blogList";
    }

    @RequestMapping(value = "/input" ,method = RequestMethod.GET)
    public String inputPage(Model model){
        model.addAttribute("blog",new BlogDetailDTO());
        List<Type> types = typeService.getAllType();
        model.addAttribute("types",types);
        List<Tag> tags = tagsService.getAllTag();
        model.addAttribute("tags",tags);
        return "admin/blogs-input";
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public String saveBlog(BlogDetailDTO blogDetailDTO, RedirectAttributes attributes){
        int count = blogService.saveBlog(blogDetailDTO);
        if (count == 1) {
            attributes.addFlashAttribute("message", "操作成功");
        } else {
            attributes.addFlashAttribute("message", "操作失败");
        }
        return "redirect:/admin/blogs?pageNum=1";
    }

    @RequestMapping(value = "{id}/input",method = RequestMethod.GET)
    public String updetePage(@PathVariable Long id,Model model){
        BlogDetailDTO blogDetailDTO = blogService.getBlogById(id);
        model.addAttribute("blog",blogDetailDTO);
        List<Type> types = typeService.getAllType();
        model.addAttribute("types",types);
        List<Tag> tags = tagsService.getAllTag();
        model.addAttribute("tags",tags);
        return "admin/blogs-input";
    }

    @RequestMapping(value = "{id}/delete",method = RequestMethod.GET)
    public String delete(@PathVariable Long id,RedirectAttributes attributes){
        int count = blogService.deleteBlog(id);
        if (count == 1) {
            attributes.addFlashAttribute("message", "删除成功");
        } else {
            attributes.addFlashAttribute("message", "删除失败");
        }
        return "redirect:/admin/blogs?pageNum=1";
    }
}

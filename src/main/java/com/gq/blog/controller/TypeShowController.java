package com.gq.blog.controller;

import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.BlogDetailDTO;
import com.gq.blog.dto.BlogSearchDTO;
import com.gq.blog.dto.TypeBlogDTO;
import com.gq.blog.pojo.Type;
import com.gq.blog.service.BlogService;
import com.gq.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class TypeShowController {

    @Autowired
    TypeService typeService;
    @Autowired
    BlogService blogService;

    @RequestMapping(value = "/types/{id}/{pageNum}",method = RequestMethod.GET)
    public String typePage(@PathVariable Long id,@PathVariable int pageNum, Model model){
        int pageSize = 8;
        List<TypeBlogDTO> typeBlogs = typeService.getTypeBlogs();
        BlogSearchDTO blogSearchDTO = new BlogSearchDTO();
        blogSearchDTO.setTypeId(id);
        blogSearchDTO.setPageNum(String.valueOf(pageNum));
        if(id==-1){
            blogSearchDTO.setTypeId(typeBlogs.get(0).getId());
        }
        PageInfo<BlogDetailDTO> blogPage = blogService.getBlogPage(blogSearchDTO, pageSize);
        model.addAttribute("types",typeBlogs);
        model.addAttribute("page",blogPage);
        model.addAttribute("activeTypeId",blogSearchDTO.getTypeId());
        return "/types";
    }
}

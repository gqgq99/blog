package com.gq.blog.controller;

import com.github.pagehelper.PageInfo;
import com.gq.blog.pojo.User;
import com.gq.blog.service.TypeService;
import com.gq.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/admin")
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    HttpSession session;
    @Autowired
    TypeService typeService;

    @RequestMapping(value = "",method = RequestMethod.GET)
    public String loginPage(){
        return "admin/login";
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String login(@RequestParam String username,
                        @RequestParam String password,
                        RedirectAttributes attribute){
        User user = userService.getUser(username, password);
        if(user!=null){
            user.setPassword("");
            session.setAttribute("user",user);
            return "admin/index";
        }else {
            String message = "用户名或密码输入错误！";
            attribute.addFlashAttribute("message",message);
            return "redirect:/admin";
        }
    }
    @RequestMapping(value = "/logout",method = RequestMethod.GET)
    public String logout(){
        session.removeAttribute("user");
        return "redirect:/admin";
    }

}

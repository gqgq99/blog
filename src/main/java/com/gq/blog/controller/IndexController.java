package com.gq.blog.controller;

import com.github.pagehelper.PageInfo;
import com.gq.blog.dto.BlogDetailDTO;
import com.gq.blog.dto.TagBlogDTO;
import com.gq.blog.dto.TypeBlogDTO;
import com.gq.blog.pojo.Blog;
import com.gq.blog.service.BlogService;
import com.gq.blog.service.TagsService;
import com.gq.blog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    BlogService blogService;
    @Autowired
    TypeService typeService;
    @Autowired
    TagsService tagsService;
    @RequestMapping(value = "/")
    public String index( Model model){
        int pageNum=1;
        int pageSize = 5;
        PageInfo<BlogDetailDTO> pageInfo = blogService.getBlogList(pageNum,pageSize);
        model.addAttribute("page",pageInfo);
        List<TypeBlogDTO> typeBlogDTOList = typeService.getTypeBlogs();
        model.addAttribute("types",typeBlogDTOList);
        List<TagBlogDTO> tagBlogDTOLis = tagsService.getTagBlogs();
        model.addAttribute("tags",tagBlogDTOLis);
        List<Blog> recommendBlogs = blogService.getRecommendBlogs(1,6);
        model.addAttribute("recommendBlogs",recommendBlogs);
        return "/index";
    }
    @RequestMapping(value = "/page/{pageNum}")
    public String indexPage(@PathVariable int pageNum, Model model){
        int pageSize = 5;
        PageInfo<BlogDetailDTO> pageInfo = blogService.getBlogList(pageNum,pageSize);
        model.addAttribute("page",pageInfo);
        List<TypeBlogDTO> typeBlogDTOList = typeService.getTypeBlogs();
        model.addAttribute("types",typeBlogDTOList);
        List<TagBlogDTO> tagBlogDTOLis = tagsService.getTagBlogs();
        model.addAttribute("tags",tagBlogDTOLis);
        List<Blog> recommendBlogs = blogService.getRecommendBlogs(1,6);
        model.addAttribute("recommendBlogs",recommendBlogs);
        return "/index";
    }

    @RequestMapping(value = "/search",method = RequestMethod.POST)
    public String search(int pageNum,@RequestParam(value = "query") String queryKey,Model model){
        int pageSize = 10;
        PageInfo<BlogDetailDTO> blogs = blogService.getBlogByQueryKey(pageNum,pageSize,queryKey);
        model.addAttribute("page",blogs);
        model.addAttribute("query",queryKey);
        return "/search";
    }

    @RequestMapping(value = "/blog/{id}",method = RequestMethod.GET)
    public String getBlog(@PathVariable Long id,Model model){
        BlogDetailDTO blogDetailDTO = blogService.getBlogByIdAndConvert(id);
        model.addAttribute("blog",blogDetailDTO);
        return "/blog";
    }
    @RequestMapping(value = "/about",method = RequestMethod.GET)
    public String about() {
        return "about";
    }
    @RequestMapping(value = "/footer/newblog",method = RequestMethod.GET)
    public String newblogs(Model model) {
        model.addAttribute("newblogs", blogService.getRecommendBlogs(1,3));
        return "_fragments :: newblogList";
    }
}

package com.gq.blog.controller;

import com.gq.blog.dto.CommentShowDTO;
import com.gq.blog.pojo.User;
import com.gq.blog.service.BlogService;
import com.gq.blog.service.CommentService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class CommentController {

    @Autowired
    BlogService blogService;
    @Autowired
    CommentService commentService;
    @Value("${comment.avatar}")
    private  String avatar;

    @RequestMapping(value = "/comments")
    public String saveComment(CommentShowDTO commentShowDTO , HttpSession session){
        commentShowDTO.setBlogId(commentShowDTO.getBlog().getId());
        commentShowDTO.setBlog(blogService.getBlogById(commentShowDTO.getBlogId()));
        User user = (User) session.getAttribute("user");
        if (user != null) {
            commentShowDTO.setAvatar(user.getAvatar());
            commentShowDTO.setAdminComment(true);
        } else {
            commentShowDTO.setAvatar(avatar);
        }
        commentService.saveComment(commentShowDTO);
        return "redirect:/comments/" + commentShowDTO.getBlogId();
    }

    @RequestMapping(value = "/comments/{blogId}")
    public String getCommentByBlogId(@PathVariable Long blogId, Model model){
        List<CommentShowDTO> commentShowDTO = commentService.getCommentByBlogId(blogId);
        model.addAttribute("comments",commentShowDTO);
        System.out.println(commentShowDTO);
        return "blog :: commentList";
    }
}

package com.gq.blog.controller;

import com.gq.blog.pojo.Blog;
import com.gq.blog.service.BlogService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

@Controller
public class ArchivesController {

    @Autowired
    BlogService blogService;

    @RequestMapping(value = "/archives",method = RequestMethod.GET)
    public String archivePage(Model model){
        Map<String, List<Blog>> map = blogService.archiveBlog();
        int count = blogService.getBlogCount();
        model.addAttribute("archiveMap",map);
        model.addAttribute("blogCount",count);
        return "/archives";
    }
}

package com.gq.blog.mapper;

import com.gq.blog.dto.BlogDetailDTO;
import com.gq.blog.dto.BlogSearchDTO;
import com.gq.blog.pojo.Blog;
import com.gq.blog.pojo.BlogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlogMapper {
    long countByExample(BlogExample example);

    int deleteByExample(BlogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Blog record);

    int insertSelective(Blog record);

    List<Blog> selectByExample(BlogExample example);

    Blog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Blog record, @Param("example") BlogExample example);

    int updateByExample(@Param("record") Blog record, @Param("example") BlogExample example);

    int updateByPrimaryKeySelective(Blog record);

    int updateByPrimaryKey(Blog record);

    List<BlogDetailDTO> getBlog(BlogSearchDTO blogSearchDTO);

    BlogDetailDTO getBlogById(Long id);

    List<BlogDetailDTO> getBlogList();

    List<Blog> getRecommendBlogs();

    List<BlogDetailDTO> getBlogByQueryKey(String queryKey);

    List<BlogDetailDTO> getBlogPageByTageId(Long id);

    List<String> getBlogYear();

    List<Blog> getBlogByTime(String year);

    int getBlogCount();
}
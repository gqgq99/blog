package com.gq.blog.mapper;

import com.gq.blog.pojo.BlogTags;
import com.gq.blog.pojo.BlogTagsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlogTagsMapper {
    long countByExample(BlogTagsExample example);

    int deleteByExample(BlogTagsExample example);

    int deleteByPrimaryKey(Long blogsId);

    int insert(BlogTags record);

    int insertSelective(BlogTags record);

    List<BlogTags> selectByExample(BlogTagsExample example);

    BlogTags selectByPrimaryKey(Long blogsId);

    int updateByExampleSelective(@Param("record") BlogTags record, @Param("example") BlogTagsExample example);

    int updateByExample(@Param("record") BlogTags record, @Param("example") BlogTagsExample example);

    int updateByPrimaryKeySelective(BlogTags record);

    int updateByPrimaryKey(BlogTags record);
}